use std::str::FromStr;

use rustyline::error::ReadlineError;
use rustyline::Editor;

use libdoodler::FlatTree;

fn main() {
    pretty_env_logger::init();

    println!("doodler v 0.0.1\nPress Ctrl-C to exit.");
    let mut rl = Editor::<()>::new().unwrap();
    loop {
        let readline = rl.readline(">> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                println!("Debug: {:?}", FlatTree::from_str(&line).ok().unwrap());
                println!("Displ: {}", FlatTree::from_str(&line).ok().unwrap());
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}
