use std::{
    fmt::{self, Display, Formatter},
    str::FromStr,
};

use crate::{FlatTree, MatchedFlatTree as Match, ParseFlatTreeError};

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Expr {
    raw: String,
    tree: FlatTree,
}

impl Display for Expr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.raw)
    }
}

impl FromStr for Expr {
    type Err = ParseFlatTreeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Expr {
            raw: s.to_string(),
            tree: FlatTree::from_str(s)?,
        })
    }
}

impl Expr {
    pub fn recognizes(&self, rhs: &Expr) -> bool {
        self.tree.recognizes(&rhs.tree)
    }

    pub fn try_recognize<'a>(&'a self, rhs: &'a Expr) -> Result<Match<'a>, ()> {
        self.tree.try_recognize(&rhs.tree)
    }
}
