pub trait Tree<TreeIndex, NodeDepth, NodeValue> {
    fn len(&self) -> usize;

    /// Gets the value stored at the given `index` in the tree.
    fn value(&self, index: TreeIndex) -> NodeValue;

    /// Gets the depth of the node at the given `index` in the tree.
    fn depth(&self, index: TreeIndex) -> NodeDepth;

    /// Gets the index of the parent of the node at the given `index` in the
    /// tree, if one exists.
    fn parent(&self, index: TreeIndex) -> Option<TreeIndex>;

    /// Gets the indices direct children of the node at the given `index`.
    fn children(&self, index: TreeIndex) -> Vec<TreeIndex>;

    /// Gets the indices of every node for which the node at the given `index`
    /// is an ancestor.
    fn descendents(&self, index: TreeIndex) -> Vec<TreeIndex>;

    /// Creates a new tree sharing the structure and values of the subtree
    /// starting at the given `index`.
    fn subtree(&self, index: TreeIndex) -> Self;

    /// Replaces the subtree at the given `index` in this tree with the given
    /// `subtree`.
    fn replace(&self, subtree: Self, index: TreeIndex) -> Self;
}

pub trait Recognize<'a, Rhs = &'a Self> {
    /// Returns whether `rhs` is "recognized" by `self`.
    ///
    /// This trait is used for types that can
    fn recognizes(&self, rhs: Rhs) -> bool;
}
