use std::fmt;
use std::fmt::{Debug, Display, Formatter};

use crate::expression::Expr;

/// Represents a rewrite rule for transforming an S-expression.
///
/// Rewrite rules are defined by their input and resulting output, expressed as
/// S-expressions.
///
/// An example of a rewrite rule for describing additive commutation is:
///   (+ a b) => (+ b a)
/// This rule could be applied to `(+ 1 2)` to produce the expression `(+ 2 1)`.
///
/// The shape of the output can be different than the shape of the input. For
/// example, the following is a legal rewrite rule:
///   (+ a 0) => a
/// Applying this rule to `(+ (* a b) 0)` would produce `(* a b)`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Rule {
    pub name: String,
    pub input: Expr,
    pub output: Expr,
}

impl Display for Rule {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let tab = "  ";
        write!(
            f,
            "(rule `{}`\n{}{} => {})",
            self.name, tab, self.input, self.output
        )
    }
}

impl Rule {
    pub fn recognizes(&self, target: &Expr) -> bool {
        self.input.recognizes(target)
    }
}
