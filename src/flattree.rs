use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
    str::FromStr,
};

use unicode_segmentation::UnicodeSegmentation;

use crate::traits::Tree;

#[derive(Debug, Clone, PartialEq, Eq)]
enum Node {
    Symbol(String),
    Integer(isize),
}

impl Display for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Node::Symbol(s) => write!(f, "{s}"),
            Node::Integer(n) => write!(f, "{n}"),
        }
    }
}

type Depth = usize;
type TreeIdx = usize;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FlatTree {
    values: Vec<Node>,
    depths: Vec<Depth>,
}

struct ZippedFlatTree<'a> {
    pattern: &'a FlatTree,
    target: &'a FlatTree,
    zipped_indices: Vec<(TreeIdx, TreeIdx)>,
}

pub struct MatchedFlatTree<'a> {
    pattern: &'a FlatTree,
    target: &'a FlatTree,
    symbol_map: HashMap<String, TreeIdx>,
}

impl<'a> TryFrom<ZippedFlatTree<'a>> for MatchedFlatTree<'a> {
    type Error = ();

    fn try_from(value: ZippedFlatTree<'a>) -> Result<Self, Self::Error> {
        use Node::*;

        let mut symbol_map: HashMap<String, TreeIdx> = HashMap::new();

        for &(pattern_idx, target_idx) in value.zipped_indices.iter() {
            let pattern_val = value.pattern.values[pattern_idx].clone();
            let target_val = value.target.values[target_idx].clone();
            match (pattern_val.clone(), target_val.clone()) {
                (Symbol(sym), Symbol(_)) => {
                    if value.pattern.children(pattern_idx).is_empty() {
                        if let Some(&cached_idx) = symbol_map.get(&sym) {
                            if !value.target.subtrees_equal(target_idx, cached_idx) {
                                return Err(());
                            }
                        } else {
                            symbol_map.insert(sym, target_idx);
                        }
                    }
                }
                (Symbol(sym), target_value) => {
                    if let Some(&cached_idx) = symbol_map.get(&sym) {
                        if value.target.values[cached_idx] != target_value {
                            return Err(());
                        }
                    } else {
                        symbol_map.insert(sym, target_idx);
                    }
                }
                _ => continue,
            }
        }

        Ok(MatchedFlatTree {
            pattern: value.pattern,
            target: value.target,
            symbol_map,
        })
    }
}

impl Tree<TreeIdx, Depth, Node> for FlatTree {
    fn len(&self) -> usize {
        self.values.len()
    }

    fn value(&self, index: TreeIdx) -> Node {
        self.values[index].clone()
    }

    fn depth(&self, index: TreeIdx) -> Depth {
        self.depths[index]
    }

    fn parent(&self, index: TreeIdx) -> Option<TreeIdx> {
        match index {
            0 => None,
            mut cursor => {
                let idx_depth = self.depths[index];
                while cursor >= std::usize::MIN {
                    if self.depths[cursor] == idx_depth - 1 {
                        return Some(cursor);
                    }
                    cursor -= 1;
                }
                None
            }
        }
    }

    fn children(&self, index: TreeIdx) -> Vec<TreeIdx> {
        let min_depth = self.depths[index];
        self.descendents_helper(index)
            .filter_map(|(i, &depth)| (depth == min_depth + 1).then_some(i))
            .collect()
    }

    fn descendents(&self, index: TreeIdx) -> Vec<TreeIdx> {
        self.descendents_iter(index).collect()
    }

    fn subtree(&self, index: TreeIdx) -> Self {
        let indices: Vec<TreeIdx> = self.subtree_helper(index).collect();
        let min_depth = self.depths[indices[0]];

        let values = indices.iter().map(|&i| self.value(i)).collect::<Vec<_>>();
        let depths = indices
            .iter()
            .map(|&i| self.depth(i) - min_depth)
            .collect::<Vec<_>>();

        Self { values, depths }
    }

    fn replace(&self, subtree: Self, index: TreeIdx) -> Self {
        let mut values = self.values.clone();
        let mut depths = self.depths.clone();
        let target_depth = self.depth(index);

        for _ in self.subtree_helper(index) {
            values.remove(index);
            depths.remove(index);
        }

        for i in 0..subtree.len() {
            values.insert(index + i, subtree.value(i));
            depths.insert(index + i, target_depth + subtree.depth(i));
        }

        Self { values, depths }
    }
}

// clean code is my passion :)
impl Display for FlatTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut last_depth = 0;
        for index in 0..self.len() {
            let depth = self.depth(index);
            if depth < last_depth {
                for _ in depth..last_depth {
                    write!(f, ")")?;
                }
                write!(f, " ")?;
            }
            last_depth = depth;

            if index < self.len() - 1 && depth < self.depth(index + 1) {
                write!(f, "(")?;
            }

            write!(f, "{}", self.value(index))?;
            if index < self.len() - 1 {
                let next_depth = self.depth(index + 1);
                if depth <= next_depth {
                    write!(f, " ")?;
                }
            }
        }

        for _ in 0..last_depth {
            write!(f, ")")?;
        }

        Ok(())
    }
}

#[allow(unused)]
impl FlatTree {
    fn level_order_indices(&self) -> Vec<TreeIdx> {
        let max_depth = self.depths.iter().max().map(|d| *d).unwrap_or_default();
        (0..=max_depth)
            .flat_map(|current_depth| {
                self.depths
                    .iter()
                    .enumerate()
                    .filter_map(move |(idx, &d)| (d == current_depth).then_some(idx))
            })
            .collect::<Vec<_>>()
    }

    fn descendents_helper(&self, idx: TreeIdx) -> impl Iterator<Item = (TreeIdx, &Depth)> + '_ {
        let min_depth = self.depths[idx];
        self.depths
            .iter()
            .enumerate()
            .skip(idx + 1)
            .take_while(move |(_, &depth)| depth > min_depth)
    }

    fn subtree_helper(&self, idx: TreeIdx) -> impl Iterator<Item = TreeIdx> + '_ {
        let min_depth = self.depths[idx];
        self.depths
            .iter()
            .enumerate()
            .skip(idx)
            .map(move |(i, depth)| (i, depth, i == idx))
            .take_while(move |(_, &depth, is_first)| *is_first || depth > min_depth)
            .map(|(i, _, _)| i)
    }

    /// Returns whether the subtrees starting at the given indices have identical values
    fn subtrees_equal(&self, idx0: TreeIdx, idx1: TreeIdx) -> bool {
        self.subtree_helper(idx0)
            .map(|i| self.value(i))
            .zip(self.subtree_helper(idx1).map(|i| self.value(i)))
            .all(|(node0, node1)| node0 == node1)
    }

    /// Zips this tree's indices with those of another.
    ///
    /// Returns Err when the two trees are incompatibly shaped for being zipped.
    fn zip_indices_with(&self, rhs: &FlatTree) -> Option<Vec<(TreeIdx, TreeIdx)>> {
        let mut zipped = Vec::with_capacity(self.values.len());
        let mut self_indices_iter = self.level_order_indices().into_iter();
        let mut rhs_indices_iter = rhs.level_order_indices().into_iter();

        let mut last_self_depth = 0;
        loop {
            match (self_indices_iter.next(), rhs_indices_iter.next()) {
                (Some(_), None) => return None,
                (None, Some(rhs_idx)) if rhs.depths[rhs_idx] == last_self_depth => return None,
                (None, Some(rhs_idx)) => break,
                (None, None) => break,
                (Some(self_idx), Some(rhs_idx)) => {
                    if self.parent(self_idx) != rhs.parent(rhs_idx) {
                        return None;
                    }
                    last_self_depth = self.depths[self_idx];
                    zipped.push((self_idx, rhs_idx))
                }
            }
        }

        Some(zipped)
    }

    fn try_zip_with<'a>(&'a self, rhs: &'a FlatTree) -> Result<ZippedFlatTree, ()> {
        use Node::*;

        self.zip_indices_with(rhs)
            .map_or(Err(()), move |zipped_indices| {
                for &(self_idx, rhs_idx) in zipped_indices.iter() {
                    let self_val = self.values[self_idx].clone();
                    let rhs_val = rhs.values[rhs_idx].clone();

                    let type_mismatch = match (self_val.clone(), rhs_val.clone()) {
                        (Integer(_), Symbol(_)) => true,
                        _ => false,
                    };

                    let integer_mismatch = match (self_val.clone(), rhs_val.clone()) {
                        (Integer(int0), Integer(int1)) if int0 != int1 => true,
                        _ => false,
                    };

                    let symbol_mismatch = match (self_val.clone(), rhs_val.clone()) {
                        (Symbol(sym0), Symbol(sym1))
                            if !self.children(self_idx).is_empty() && sym0 != sym1 =>
                        {
                            true
                        }
                        _ => false,
                    };

                    if type_mismatch || integer_mismatch || symbol_mismatch {
                        return Err(());
                    }
                }
                Ok(ZippedFlatTree {
                    pattern: self,
                    target: rhs,
                    zipped_indices,
                })
            })
    }

    /// Gets the indices of every node that is a descendent of this node as an iterator.
    pub fn descendents_iter(&self, idx: TreeIdx) -> impl Iterator<Item = TreeIdx> + '_ {
        self.descendents_helper(idx).map(|(i, _)| i)
    }

    pub fn try_recognize<'a>(&'a self, rhs: &'a FlatTree) -> Result<MatchedFlatTree<'a>, ()> {
        self.try_zip_with(rhs)?.try_into()
    }

    pub fn recognizes(&self, rhs: &FlatTree) -> bool {
        self.try_recognize(rhs).is_ok()
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum ParseFlatTreeErrorType {
    MismatchedParens,
    MissingOp,
    ExtraneousTokens,
    MalformedExpression,
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParseFlatTreeError {
    pub ty: ParseFlatTreeErrorType,
}

impl Display for ParseFlatTreeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.ty {
            ParseFlatTreeErrorType::MismatchedParens => write!(f, "Mismatched parentheses."),
            ParseFlatTreeErrorType::MissingOp => write!(
                f,
                "Found literal at start of S-expression but was expecting an operator symbol."
            ),
            ParseFlatTreeErrorType::ExtraneousTokens => {
                write!(f, "Encountered extraneous tokens at the end of expression.")
            }
            ParseFlatTreeErrorType::MalformedExpression => {
                write!(f, "Encountered a malformed S-expression.")
            }
        }
    }
}

impl std::error::Error for ParseFlatTreeError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }

    fn description(&self) -> &str {
        "description() is deprecated; use Display"
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.source()
    }
}

impl ParseFlatTreeError {
    pub fn new(ty: ParseFlatTreeErrorType) -> ParseFlatTreeError {
        ParseFlatTreeError { ty }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum ExprParserState {
    ExpectingExpr,
    ExpectingExprOrRParen,
    ExpectingOp,
    ExpectingEOE,
}

impl FromStr for FlatTree {
    type Err = ParseFlatTreeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ExprParserState::*;
        use ParseFlatTreeErrorType::*;

        fn push_value(token: &str, depth: Depth, values: &mut Vec<Node>, depths: &mut Vec<Depth>) {
            let value = token
                .parse::<isize>()
                .map_or_else(|_| Node::Symbol(token.to_string()), Node::Integer);
            values.push(value);
            depths.push(depth);
        }

        const LPAREN: &str = "(";
        const RPAREN: &str = ")";
        const SPACE: &str = " ";

        let mut values = vec![];
        let mut depths = vec![];

        let mut state = ExpectingExpr;
        let mut depth = 0;

        for token in s.split_word_bounds().filter(|&tok| tok != SPACE) {
            match (state, token) {
                (ExpectingExpr, RPAREN) => return Err(ParseFlatTreeError::new(MismatchedParens)),
                (ExpectingExpr, LPAREN) => state = ExpectingOp,
                (ExpectingExpr, token) => push_value(token, depth, &mut values, &mut depths),
                (ExpectingExprOrRParen, LPAREN) => state = ExpectingOp,
                (ExpectingExprOrRParen, RPAREN) if depth == 0 => {
                    return Err(ParseFlatTreeError::new(MismatchedParens));
                }
                (ExpectingExprOrRParen, RPAREN) => {
                    depth -= 1;
                    if depth == 0 {
                        state = ExpectingEOE;
                    }
                }
                (ExpectingExprOrRParen, token) => {
                    push_value(token, depth, &mut values, &mut depths)
                }
                (ExpectingOp, LPAREN) => return Err(ParseFlatTreeError::new(MissingOp)),
                (ExpectingOp, RPAREN) => return Err(ParseFlatTreeError::new(MissingOp)),
                (ExpectingOp, token) if token.parse::<isize>().is_ok() => {
                    return Err(ParseFlatTreeError::new(MissingOp));
                }
                (ExpectingOp, token) => {
                    push_value(token, depth, &mut values, &mut depths);
                    depth += 1;
                    state = ExpectingExprOrRParen;
                }
                (ExpectingEOE, _) => {
                    return Err(ParseFlatTreeError::new(ExtraneousTokens));
                }
            }
        }

        if depth != 0 {
            return Err(ParseFlatTreeError::new(MismatchedParens));
        }

        Ok(FlatTree { values, depths })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;
    use Node::*;

    #[test]
    fn from_str() {
        let tree: FlatTree = FlatTree::from_str("(+ (+ 1 2) 3)").unwrap();
        assert_eq!(
            tree.values,
            vec![
                Symbol("+".into()),
                Symbol("+".into()),
                Integer(1),
                Integer(2),
                Integer(3)
            ]
        );
        assert_eq!(tree.depths, vec![0, 1, 2, 2, 1]);
    }

    #[test]
    fn descendents() {
        let tree = FlatTree::from_str("(+ (+ (+ 1 2) 3) 4)").unwrap();
        assert_eq!(tree.descendents(1), vec![2, 3, 4, 5]);
    }

    #[test]
    fn children() {
        let tree = FlatTree::from_str("(+ (+ (+ 1 2) 3) 4)").unwrap();
        assert_eq!(tree.children(1), vec![2, 5]);
    }

    #[test]
    fn parents() {
        let tree = FlatTree::from_str("(+ (+ (+ a b) c) (+ d e))").unwrap();
        let parents = (0..tree.values.len())
            .map(|idx| tree.parent(idx))
            .collect::<Vec<Option<usize>>>();
        assert_eq!(
            parents,
            vec![
                None,
                Some(0),
                Some(1),
                Some(2),
                Some(2),
                Some(1),
                Some(0),
                Some(6),
                Some(6)
            ]
        );
    }

    #[test]
    fn subtree_helper() {
        let tree = FlatTree::from_str("(+ (+ a b) (+ c d))").unwrap();
        assert_eq!(tree.subtree_helper(1).collect::<Vec<_>>(), vec![1, 2, 3]);
    }

    #[test]
    fn subtree() {
        let tree = FlatTree::from_str("(+ (+ a b) (+ c d))").unwrap();
        let expected = FlatTree::from_str("(+ a b)").unwrap();
        assert_eq!(tree.subtree(1), expected);
    }

    #[test]
    fn replace_single_element_with_single_element() {
        let tree = FlatTree::from_str("(+ a b)").unwrap();
        let replacement = FlatTree::from_str("100").unwrap();
        let expected = FlatTree::from_str("(+ 100 b)").unwrap();
    }

    #[test]
    fn replace_subtree_with_single_element() {
        let tree = FlatTree::from_str("(+ (+ a b) (+ c d))").unwrap();
        let replacement = FlatTree::from_str("100").unwrap();
        let expected = FlatTree::from_str("(+ 100 (+ c d))").unwrap();
        assert_eq!(tree.replace(replacement, 1), expected);
    }

    #[test]
    fn replace_subtree_with_subtree() {
        let tree = FlatTree::from_str("(+ (+ a b) (+ c d))").unwrap();
        let replacement = FlatTree::from_str("(+ 1 (+ 2 3))").unwrap();
        let expected = FlatTree::from_str("(+ (+ 1 (+ 2 3)) (+ c d))").unwrap();
        assert_eq!(tree.replace(replacement, 1), expected);
    }

    #[test]
    fn level_order_indices() {
        let tree = FlatTree::from_str("(+ (+ (+ 1 2) 3) 4)").unwrap();
        assert_eq!(tree.level_order_indices(), vec![0, 1, 6, 2, 5, 3, 4]);
    }

    #[test]
    fn zip_with_same_shape_ok() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ 1 1)").unwrap();
        let zipped: Vec<(TreeIdx, TreeIdx)> = pattern.zip_indices_with(&target).unwrap();
        assert_eq!(zipped, vec![(0, 0), (1, 1), (2, 2)]);
    }

    #[test]
    fn zip_with_mixed_shape_ok() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ (+ 1 2) (+ 1 2))").unwrap();
        let zipped: Vec<(TreeIdx, TreeIdx)> = pattern.zip_indices_with(&target).unwrap();
        assert_eq!(zipped, vec![(0, 0), (1, 1), (2, 4)]);
    }

    #[test]
    fn recognizes_pattern_too_big_should_fail() {
        let pattern = FlatTree::from_str("(+ a a a)").unwrap();
        let target = FlatTree::from_str("(+ 1 1)").unwrap();
        assert!(!pattern.recognizes(&target));
    }

    #[test]
    fn recognizes_pattern_too_small_should_fail() {
        let pattern = FlatTree::from_str("(+ a)").unwrap();
        let target = FlatTree::from_str("(+ 1 1)").unwrap();
        assert!(!pattern.recognizes(&target));
    }

    #[test]
    fn recognizes_children_of_different_parents_should_fail() {
        let pattern = FlatTree::from_str("(+ (+ a b c) (+ d e))").unwrap();
        let target = FlatTree::from_str("(+ (+ a b) (+ c d e))").unwrap();
        assert!(!pattern.recognizes(&target));
        assert!(!target.recognizes(&pattern));
    }

    #[test]
    fn recognizes_with_same_shape_true() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ 1 1)").unwrap();
        assert!(pattern.recognizes(&target));
    }

    #[test]
    fn recognizes_with_same_shape_but_mismatched_values_false() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ 1 2)").unwrap();
        assert!(!pattern.recognizes(&target));
    }

    #[test]
    fn recognizes_with_mixed_shape_true() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ (+ 1 2) (+ 1 2))").unwrap();
        assert!(pattern.recognizes(&target));
    }

    #[test]
    fn recognizes_with_mixed_shape_but_mismatched_values_false() {
        let pattern = FlatTree::from_str("(+ a a)").unwrap();
        let target = FlatTree::from_str("(+ (+ 1 2) (+ 3 4))").unwrap();
        assert!(!pattern.recognizes(&target));
    }

    mod parse {
        use super::ParseFlatTreeErrorType::*;
        use super::*;

        #[test]
        fn simple_expr() {
            let expected = Ok(FlatTree {
                values: vec![Node::Integer(1)],
                depths: vec![0],
            });
            assert_eq!(FlatTree::from_str("1"), expected);
        }

        #[test]
        fn compound_expr() {
            let expected = Ok(FlatTree {
                values: vec![
                    Node::Symbol("+".to_string()),
                    Node::Integer(1),
                    Node::Integer(2),
                ],
                depths: vec![0, 1, 1],
            });
            assert_eq!(FlatTree::from_str("(+ 1 2)"), expected);
        }

        #[test]
        fn nested_expr() {
            let expected = Ok(FlatTree {
                values: vec![
                    Node::Symbol("+".to_string()),
                    Node::Symbol("+".to_string()),
                    Node::Integer(1),
                    Node::Integer(2),
                    Node::Integer(3),
                ],
                depths: vec![0, 1, 2, 2, 1],
            });
            assert_eq!(FlatTree::from_str("(+ (+ 1 2) 3)"), expected);
        }

        #[test]
        fn missing_op() {
            let expected = Err(ParseFlatTreeError::new(MissingOp));
            assert_eq!(FlatTree::from_str("(1 2 3)"), expected);
        }

        #[test]
        fn missing_rparen() {
            let expected = Err(ParseFlatTreeError::new(MismatchedParens));
            assert_eq!(FlatTree::from_str("(+ 1 2"), expected);
        }

        #[test]
        fn extraneous_tokens() {
            let expected = Err(ParseFlatTreeError::new(ExtraneousTokens));
            assert_eq!(FlatTree::from_str("(+ 1 2))"), expected);
            assert_eq!(FlatTree::from_str("(+ 1 2)(+ 3 4)"), expected);
        }

        #[test]
        fn missing_operator() {
            let expected = Err(ParseFlatTreeError::new(MissingOp));
            assert_eq!(FlatTree::from_str("()"), expected);
        }
    }
}
