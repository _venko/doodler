use std::str::FromStr;

use libdoodler::*;

#[test]
fn sample_integration_test() -> Result<(), Box<dyn std::error::Error>> {
    let target = Expr::from_str("(+ 1 1)")?;
    let rule = Rule {
        name: "Double".into(),
        input: Expr::from_str("(+ a a)")?,
        output: Expr::from_str("(* 2 a)")?,
    };
    assert!(rule.recognizes(&target));
    Ok(())
}
