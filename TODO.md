# TODOs
* ~~Implement "matching" against SExprs.~~
* Implement parsing Rule definitions as SExprs.
* Investigate what might be missing for defining rewrite rules cleanly
  * Investigate adding non-numeric literals into S-expressions
* Implement SExpr pretty-printing since we're also using it to parse and display Rules
  * Once done, reimpl Display for Rule
